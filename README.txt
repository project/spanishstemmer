
Readme
------

This module implements a stemmer algorithm to improve Spanish language
searching with the Drupal built-in search.module.

It reduces each word in the index to its basic root or stem (e.g. 'blogging' to
'blog') so that variations on a word ('blogs', 'blogger', 'blogging', 'blog') are
considered equivalent when searching. This generally results in more relevant
results.


